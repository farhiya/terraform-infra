resource "aws_vpc" "my-vpc"{
    cidr_block = var.vpc_cidr
    tags = {
        Name = "${var.my_tag}-terraform-aws-vpc"
    }
}

resource "aws_internet_gateway" "my-internet-gateway" {
    vpc_id =  aws_vpc.my-vpc.id
    tags = {
        Name = "${var.my_tag}-IGW"
 }
  
}

resource "aws_network_acl" "my-nacl" {
  vpc_id = aws_vpc.my-vpc.id
  subnet_ids = [aws_subnet.public-subnet.id, aws_subnet.public-subnet-2.id]
 

  egress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
    }

  ingress {
      protocol   = -1
      rule_no    = 100
      action     = "allow"
      cidr_block = "0.0.0.0/0"
      from_port  = 0
      to_port    = 0
  }

  tags = {
    Name = "${var.my_tag}_NACL"
  }
}

/*
  Public Subnet
*/
resource "aws_subnet" "public-subnet" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr
    availability_zone = "${var.aws_region}a"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}_Public_Subnet"
    }
}

resource "aws_subnet" "public-subnet-2" {
    vpc_id = aws_vpc.my-vpc.id

    cidr_block = var.public_subnet_cidr_2
    availability_zone = "${var.aws_region}b"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.my_tag}_Public_Subnet_2"
    }
}

resource "aws_route_table" "my-route-table" {
    vpc_id = aws_vpc.my-vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.my-internet-gateway.id
    }

    tags = {
        Name = "${var.my_tag}_Route_Table"
    }
}

# Creating an association between a route table and a subnet or a route table and an internet gateway or virtual private gateway
resource "aws_route_table_association" "my-route-table-association" {
    subnet_id = aws_subnet.public-subnet.id
    route_table_id = aws_route_table.my-route-table.id
}

resource "aws_route_table_association" "my-route-table-association-2" {
    subnet_id = aws_subnet.public-subnet-2.id
    route_table_id = aws_route_table.my-route-table.id
}

