variable "my_tag" {
    description = "Name to tag"
    default = "farhiya"
}

variable "vpc_cidr"{
    description = "CIDR for the whole VPC"
    default = "10.0.4.0/25"
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "eu-west-3"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.4.0/27"
}

variable "public_subnet_cidr_2" {
    description = "CIDR for the Public Subnet 2"
    default = "10.0.4.96/27"
}